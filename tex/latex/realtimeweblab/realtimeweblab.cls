%% ============================================================
%% LaTeX osztály a valós idejű web laborkomponenseire
%% Szerző: Sulyok Csaba <csaba.sulyok@gmail.com>
%% ============================================================

\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{realtimeweblab}[2019/02/07 realtimeweb labor]


% kiindulópont: article.cls
\LoadClass[a4paper,10pt,oneside]{article}


% magyarosító
\RequirePackage[utf8]{inputenc}
\RequirePackage{t1enc}
\RequirePackage[magyar]{babel}


% magyar indentálás
\RequirePackage{indentfirst}


% betűtípusok
\RequirePackage{fontspec}
\setmainfont[Ligatures=TeX, 
             Extension=.ttf,
             UprightFont=*_Lt,
             ItalicFont=*_LtIt,
             BoldFont=*_Bd,
             BoldItalicFont=*_BdIt]{Aller}
\setmonofont[Ligatures=NoCommon, 
             Extension=.ttf]{Consolas}


% margó és sortávolság beállítása
\RequirePackage{geometry}
\RequirePackage{setspace}
\geometry{a4paper, left=15mm, right=15mm, top=20mm, bottom=20mm}
\linespread{1.2}


% set text colors
\RequirePackage{xcolor}
\definecolor{DarkGray}{HTML}{161616}
\definecolor{DarkBlue}{HTML}{37454d}
\definecolor{DarkBrown}{HTML}{332d2b}
\definecolor{Gray}{HTML}{e0e0e4}
\definecolor{Blue}{HTML}{93b7cc}
\definecolor{Brown}{HTML}{ccb3ab}


% címlap
\RequirePackage{textpos}
\RequirePackage{eso-pic}
\RequirePackage{tcolorbox}
\makeatletter
\def\@maketitle{%
    \null
    \vskip 5cm \par
    \begin{tcolorbox}[colback=white, colframe=DarkGray, opacityback=0.35, opacityframe=0.35, left skip=6cm, right skip=-5cm]%
        {\LARGE \bfseries \@title \par}
    \end{tcolorbox}
    \vskip 3cm}
\makeatother


% \texttt színek
\let\Oldtexttt\texttt
\renewcommand\texttt[1]{{\ttfamily\color{DarkBlue}#1}}


% url stílusok
\RequirePackage{hyperref}
\hypersetup{
  colorlinks=true,
  linkcolor=DarkBlue,
  urlcolor=DarkBlue
}


% ne számozzuk a fejezeteket
\RequirePackage{titlesec}
\titleformat{\section}{\normalfont\Large\bfseries}{}{0em}{}{}
\titleformat{\subsection}{\normalfont\large\bfseries}{}{1em}{\titlerule[0.8pt]\\}[]
\titleformat{\subsubsection}{\normalfont\normalsize\bfseries\itshape}{}{2em}{}[]


% oldalfejlécek tartalmazzák a címet és oldalszámot
\RequirePackage{fancyhdr}
\pagestyle{fancy}
\lhead{\itshape\MakeUppercase\headertitle}
\rhead{\thepage}
\cfoot{}


% kisebb itemize/enumerate skippek
\let\tempone\itemize
\let\temptwo\enditemize
\renewenvironment{itemize}{\tempone\itemsep0em\vspace{-0.5em}}{\temptwo}


% env amellyel boxot használhatunk
\newenvironment{exercise}[1]%
    {\begin{tcolorbox}[colback=Brown!20!white, colframe=DarkBrown, left skip=1.2cm, title=#1, fonttitle=\bfseries\itshape]}%
    {\end{tcolorbox}}


% bugfix
\makeatletter
\global\let\tikz@ensure@dollar@catcode=\relax
\makeatother